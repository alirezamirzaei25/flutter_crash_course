
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:session3/BlocDelegate.dart';
import 'package:session3/session4/bloc.dart';
import 'package:session3/session4/screens/BlocHandler.dart';



void main() {


  BlocSupervisor.delegate = AppBlocDelegate();

  runApp(
      MultiBlocProvider(providers: [

        BlocProvider<TestBloc>(create: (context) => TestBloc(),),

      ], child: MaterialApp(
          home: TestingPage()),)
  );


}

