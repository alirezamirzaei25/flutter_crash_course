import 'package:flutter/material.dart';
import 'package:session3/session4/bloc.dart';
import 'package:session3/session4/event.dart';

class Login extends StatefulWidget {
  TestBloc  testBloc;
  Login(this.testBloc);


  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Text("login page"),
            FlatButton(onPressed: (){
              widget.testBloc.add(GotoRegister("0938100000","this is test"));
            }, child: Text("click"))
          ],
        ),
      ),
    );
  }
}
