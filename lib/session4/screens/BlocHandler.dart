import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:session3/session4/bloc.dart';
import 'package:session3/session4/screens/Login.dart';
import 'package:session3/session4/screens/Register.dart';
import 'package:session3/session4/state.dart';

class TestingPage extends StatefulWidget {
  @override
  _TestingPageState createState() => _TestingPageState();
}

class _TestingPageState extends State<TestingPage> {


  TestBloc  testBloc;

  @override
  void initState() {
    super.initState();
    testBloc = BlocProvider.of<TestBloc>(context);
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(

      body:
      Center(
          child: BlocBuilder<TestBloc, TestState>(
            builder: (context, state) {

              if(state is  LoginInit)
                {
                  return Login(testBloc);
                }
              else if(state is RegisterInit)
                  {
                    String getString = state.str;
                    return Register(getString);
                  }
              else{
                return Scaffold(body: Center(child: Text("loading"),),);
              }

            },
          )
      ),
    );
  }





}