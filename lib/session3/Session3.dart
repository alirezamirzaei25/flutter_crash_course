

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../model/book.dart';

class MyHomePage extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Book> bookArr = new List();


  Future<List> getData() async
  {


    Dio dio = new Dio();
    final response = await dio.get("https://api.koobook.app/books/", queryParameters: {
      "fields": "url,Title,Image,Description,Publisher,Rate,Authors",
    });


    print(response.data);

    if(response.statusCode == 200)
    {
      print("hottestbooks fuction works successfuly");
      return response.data;
    }

  }



  Future <bool>  starting() async
  {
    List response = await getData();


    for(int i = 0 ; i < response.length ; i++)
    {
      Book book = new Book(response[i]['url'], response[i]['Title'], response[i]['Image'], response[i]['Publisher']);
      bookArr.add(book);
    }

    return true;

  }


  Widget hasData()
  {
    return ListView.builder(
      itemCount: bookArr.length,
      itemBuilder:(context,index){
        return showBook(bookArr[index]);
      },

    );

  }

  Widget showBook(Book book)
  {
    return Column(
      children: [
        Text("نام: ${book.title}"),
        Text("انتشارات: ${book.publisher}"),
        Image.network(book.image)
      ],
    );
  }




  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(

        child: FutureBuilder(
          future: starting(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError)
                return IconButton(icon: Icon(Icons.refresh), onPressed: (){
                  setState(() {

                  });
                });
              else {
                if (snapshot.data) {
                  return hasData();
                }
              }
            }
            return Text("loading......");
          },
        ),

      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

